import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scikits.odes import dae
from scipy import optimize

from pyocfem import OrtogonalCollocationFiniteElements


def run_dyn_sample():

    # ocfem = OrtogonalCollocationFiniteElements(10, 3)
    model = ModelAdsorptionOcfem()
    ocfem = model.ocfem_C

    idxs_algebraics = ocfem.get_algebraic_index()

    model_sample_dyn = model.residual_model

    y0 = ocfem.get_initial_guess() *0.0#
    yp0 = np.empty_like(y0)
    # res = model_sample_ss(y0, ocfem)

    SOLVER = 'ida'
    tout = np.linspace(0, 1.0, 101)
    # tout = np.linspace(0, 0.1, 200)

    model_sample_dyn(0, y0, np.zeros_like(y0), yp0)

    solver = dae('ida', model_sample_dyn,
                compute_initcond='yp0',
                #  first_step_size=1e-10,
                atol=1e-6,
                rtol=1e-6,
                algebraic_vars_idx=idxs_algebraics,
                old_api=False)

    solution = solver.solve(tout, y0, yp0)

    if solution.flag < 0:
        print ('Error: ', solution.message, 'Error at time', solution.errors.t)
        quit()


    y_time_mat = solution.values.y
    idx_in_r_grid = ocfem.get_indexes_solution_in_r_grid()
    y_time_mat_adj = y_time_mat[:, idx_in_r_grid]
    C_out = y_time_mat_adj[:,-1]

    plt.figure()
    plt.plot(tout, y_time_mat_adj)
    # plt.plot(tout, C_out)

    plt.figure()
    plt.plot(model.z, y_time_mat_adj[-1,:])

    return


class ModelAdsorptionOcfem():

    def __init__(self):
        self.Dax = 1e-9
        self.v = 0.09
        self.void_tot = 0.8
        self.C_In = 1.0
        self.L = 25e-2

        ne = 10
        self.h = np.linspace(0, self.L, ne+1)

        self.ocfem_C = OrtogonalCollocationFiniteElements(ne, 2, self.h)

        self.z = self.ocfem_C.r

        n_Cs = self.ocfem_C.n_grid
        self.n_states = self.ocfem_C.n_tot + n_Cs
        pass

    def residual_model(self, t, y, ydot, residue):
        """
            dCdt = -v dCdt + Dax d2Cdz2 - (1-e)/e dqdt
        """
        Dax = self.Dax
        v = self.v
        void_tot = self.void_tot
        C_In = self.C_In
        L = self.L

        ocfem = self.ocfem_C

        n = ocfem.n_grid

        ydot_mat = ydot.reshape((ocfem.ne,-1))
        residue_mat = residue.reshape((ocfem.ne,-1))

        y_mat, d1_mat, d2_mat = ocfem.get_matrix(y)
        cont_res = ocfem.get_continuity_residual()
        residue[0:len(cont_res)] = cont_res

        i_low = len(cont_res)

        # Boundary Condition in x=0
        residue[i_low] = Dax*d1_mat[0, 0] - (v * (y_mat[0,0] - C_In))

        # Adsorbed qtty
        dqdt = 0.0 #FIXME

        # Inner points
        c = i_low+1
        for k in range(ocfem.ne):
            for i in range(1,ocfem.n_nodal-1):
                # residue[c] = ydot_mat[k,i] + d1_mat[k,i] - (1/Pe * d2_mat[k,i] - Da*y_mat[k,i])
                residue[c] = ydot_mat[k,i] - (
                    -v*d1_mat[k,i] +
                    Dax*d2_mat[k,i] -
                    (1-void_tot)/void_tot * dqdt
                )
                c += 1

        # Boundary Conditions in x = 0
        residue[-1] = d1_mat[-1,-1] - 0.0

        # # Continuity Residues:
        # residue_mat[0,-1] = cont_res[0]
        # residue_mat[1,-2] = cont_res[1]

        pass


# def create_residual_function(ocfem):
#     Dax = 1e-9
#     v = 0.09
#     void_tot = 0.8
#     C_In = 1.0
#     L = 25e-2

#     def residual_model(t, y, ydot, residue):
#         """
#             dCdt = -v dCdt + Dax d2Cdz2 - (1-e)/e dqdt
#         """

#         n = ocfem.n_grid

#         ydot_mat = ydot.reshape((ocfem.ne,-1))
#         residue_mat = residue.reshape((ocfem.ne,-1))

#         y_mat, d1_mat, d2_mat = ocfem.get_matrix(y)
#         cont_res = ocfem.get_continuity_residual()
#         residue[0:len(cont_res)] = cont_res

#         i_low = len(cont_res)

#         # Boundary Condition in x=0
#         residue[i_low] = Dax*d1_mat[0, 0] - (v * (y_mat[0,0] - C_In))

#         # Adsorbed qtty
#         dqdt = 1.0 #FIXME

#         # Inner points
#         c = i_low+1
#         for k in range(ocfem.ne):
#             for i in range(1,ocfem.n_nodal-1):
#                 # residue[c] = ydot_mat[k,i] + d1_mat[k,i] - (1/Pe * d2_mat[k,i] - Da*y_mat[k,i])
#                 residue[c] = ydot_mat[k,i] - (
#                     -v*d1_mat[k,i] +
#                     Dax*d2_mat[k,i] -
#                     (1-void_tot)/void_tot * dqdt
#                 )
#                 c += 1

#         # Boundary Conditions in x = 0
#         residue[-1] = d1_mat[-1,-1] - 0.0

#         # # Continuity Residues:
#         # residue_mat[0,-1] = cont_res[0]
#         # residue_mat[1,-2] = cont_res[1]

#         pass
#     return residual_model


if __name__ == "__main__":
    run_dyn_sample()

    plt.show()
