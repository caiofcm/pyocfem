
#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
***********************************************************************************
                           tutorial_ocfem_cv_1.py
                DAE Tools: pyDAE module, www.daetools.com
                Copyright (C) Dragan Nikolic
***********************************************************************************
DAE Tools is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License version 3 as published by the Free Software
Foundation. DAE Tools is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with the
DAE Tools software; if not, see <http://www.gnu.org/licenses/>.
************************************************************************************
"""
__doc__ = """
Code verification using the Method of Exact Solutions.

The problem in this tutorial is 1D *transient convection-diffusion* equation (tutorial_cv_6 from daetools):

.. code-block:: none

   dc_dt + u*dc/dx - D*d2c/dc2 = 0

The equation is solved using the high resolution cell-centered finite volume upwind scheme
with flux limiter described in the article.

Numerical vs. exact solution plots varying the number of finite elements:

OCFEM Method using the auxiliary `pyocfem` package for the discretization

"""

import sys, math, numpy
from time import localtime, strftime
import matplotlib.pyplot as plt
from daetools.pyDAE import *
from daetools.solvers.superlu import pySuperLU
from pyocfem import OrtogonalCollocationFiniteElements

# Standard variable types are defined in variable_types.py
from pyUnits import m, g, kg, s, K, mol, kmol, J, um

c_t = daeVariableType("c_t", dimless, -1.0e+20, 1.0e+20, 0.0, 1e-07)

u  = 1.0
D  = 0.002
L  = 1.0
deltat = 0.3
pi = numpy.pi

class modTutorial(daeModel):
    def __init__(self, Name, Parent = None, Description = "", ocfem=None):
        daeModel.__init__(self, Name, Parent, Description)

        self.ocfem = ocfem

        self.x = daeDomain("x",  self, m, "")

        self.c = daeVariable("c", c_t, self, "C for OCFEM method in the full grid", [self.x])

    def DeclareEquations(self):
        daeModel.DeclareEquations(self)

        # xp = self.x.Points
        Nx = self.x.NumberOfPoints
        # t  = Time()
        c = lambda i: self.c(i)
        c_array = numpy.array([
            c(i)
            for i in range(Nx)
        ], dtype=object)

        ocfem = self.ocfem

        d1dx, d2dx = ocfem.get_derivatives_d1_d2(c_array)

        cont_res = ocfem.get_continuity_residual()

        for i in range(len(cont_res)):
            eq = self.CreateEquation("constrains_ocfem(%d)" % i, "")
            eq.Residual = cont_res[i]


        idx_inners = ocfem.get_indexes_inner_points().tolist()

        for i in idx_inners:
            eq = self.CreateEquation("c(%d)" % i, "")
            eq.Residual = dt(c_array[i]) + u * d1dx[i] - D * d2dx[i]
            eq.CheckUnitsConsistency = False

        # Lower Boundary:
        eq = self.CreateEquation("BC1", "")
        eq.Residual = c_array[0] - 0.0

        # Upper Boundary:
        eq = self.CreateEquation("BC2", "")
        eq.Residual = d1dx[-1] - 0.0

        pass

class simTutorial(daeSimulation):
    def __init__(self, ne):

        # ne = 10
        self.L = 1.0
        h = numpy.linspace(0, L, ne + 1)
        ocfem = OrtogonalCollocationFiniteElements(ne, 1, h, dtype=adouble)

        self.n_tot_ocfem = ocfem.n_tot

        daeSimulation.__init__(self)
        self.m = modTutorial("tutorial_cv_6(%d)" % ne, ocfem=ocfem)
        self.m.Description = __doc__

        self.ne = ne

    def SetUpParametersAndDomains(self):
        self.m.x.CreateStructuredGrid(self.n_tot_ocfem-1, 0.0, self.L)
        self.m.x.Points = self.m.ocfem.x_full

    def SetUpVariables(self):
        Nx = self.m.x.NumberOfPoints
        xp = self.m.x.Points
        idx_inner_points = self.m.ocfem.get_indexes_inner_points()
        for i in idx_inner_points.tolist():
            self.m.c.SetInitialCondition(int(i), numpy.sin(pi*xp[i]))


# Setup everything manually and run in a console
def simulate(ne, **kwargs):
    # Create Log, Solver, DataReporter and Simulation object
    log          = daePythonStdOutLog()
    daesolver    = daeIDAS()
    datareporter = daeDelegateDataReporter()
    simulation   = simTutorial(ne)

    # Do no print progress
    log.PrintProgress = False

    lasolver = pySuperLU.daeCreateSuperLUSolver()

    daesolver.RelativeTolerance = 1e-7

    # 1. TCP/IP
    tcpipDataReporter = daeTCPIPDataReporter()
    datareporter.AddDataReporter(tcpipDataReporter)
    simName = simulation.m.Name + strftime(" [%d.%m.%Y %H:%M:%S]", localtime())
    if not tcpipDataReporter.Connect("", simName):
        sys.exit()

    # 2. Data
    dr = daeNoOpDataReporter()
    datareporter.AddDataReporter(dr)

    daeActivity.simulate(simulation, reportingInterval = 0.02,
                                     timeHorizon       = deltat,
                                     lasolver          = lasolver,
                                     datareporter      = datareporter,
                                     **kwargs)

    ###########################################
    #  Data                                   #
    ###########################################
    results = dr.Process.dictVariables

    cvar = results[simulation.m.Name + '.c']
    c    = cvar.Values[-1, :]  # 2D array [t,x]

    return simulation.m.x.Points, c, simulation

def run(**kwargs):
    # Nxs = numpy.array([20, 40, 80])
    N_elements = numpy.array([1, 2, 5, 10, 20, 30])
    # N_elements = numpy.array([20])
    # n = len(N_elements)
    # hs = L / N_elements
    c  = []

    # Run simulations
    for i, ne in enumerate(N_elements):
        nx, c_, simulation = simulate(int(ne), **kwargs)
        # Exact solution:
        cexact_ = []
        for xk in nx:
            if xk >= u*deltat:
                ce = numpy.exp(-D*deltat) * numpy.sin(pi*(xk-u*deltat))
            else:
                ce = 0
            cexact_.append(ce)
        c.append((nx, c_, cexact_))

    fontsize = 14
    fontsize_legend = 11
    fig = plt.figure(figsize=(12,4), facecolor='white')
    fig.canvas.set_window_title('OCFEM Code Verification')

    for i, ne in enumerate(N_elements):
        ax = plt.subplot(2,3,i+1)
        plt.figure(1, facecolor='white')
        plt.plot(c[i][0], c[i][1], 'ro', linewidth=1.0, label='c (ne=%d)' % ne)
        plt.plot(c[i][0], c[i][2], 'b-', linewidth=1.0, label='c_exact (ne=%d)' % ne)
        plt.xlabel('x', fontsize=fontsize)
        plt.ylabel('c', fontsize=fontsize)
        plt.legend(fontsize=fontsize_legend)


    plt.tight_layout()
    plt.show()

if __name__ == "__main__":
    run()
