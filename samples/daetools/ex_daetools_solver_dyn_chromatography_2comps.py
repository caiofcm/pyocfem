
#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
***********************************************************************************
                           tutorial_cv_6.py
                DAE Tools: pyDAE module, www.daetools.com
                Copyright (C) Dragan Nikolic
***********************************************************************************
DAE Tools is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License version 3 as published by the Free Software
Foundation. DAE Tools is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with the
DAE Tools software; if not, see <http://www.gnu.org/licenses/>.
************************************************************************************
"""
__doc__ = """
Code verification using the Method of Exact Solutions.

Reference (section 3.3):

- B. Koren. A robust upwind discretization method for advection, diffusion and source terms.
  Department of Numerical Mathematics. Report NM-R9308 (1993).
  `PDF <http://oai.cwi.nl/oai/asset/5293/05293D.pdf>`_

The problem in this tutorial is 1D *transient convection-diffusion* equation:

.. code-block:: none

   dc_dt + u*dc/dx - D*d2c/dc2 = 0

The equation is solved using the high resolution cell-centered finite volume upwind scheme
with flux limiter described in the article.

Numerical vs. exact solution plots (Nx = [20, 40, 80]):

.. image:: _static/tutorial_cv_6-results.png
   :width: 800px
"""

import sys, math, numpy
from time import localtime, strftime
import matplotlib.pyplot as plt
from daetools.pyDAE import *
from daetools.solvers.superlu import pySuperLU
from pyocfem import OrtogonalCollocationFiniteElements
from dataclasses import dataclass, field
from dataclasses_json import DataClassJsonMixin

# Standard variable types are defined in variable_types.py
from pyUnits import m, g, kg, s, K, mol, kmol, J, um

# c_t = daeVariableType("c_t", dimless, -1.0e+20, 1.0e+20, 0.0, 1e-07)

u  = 1.0
D  = 0.002
L  = 1.0
deltat = 0.3
pi = numpy.pi


@dataclass
class OperationParams(DataClassJsonMixin):
    Q_In: float = 1 * 1e-6/60 # m^3/s
    epsilon: float = 0.742
    injectedInterval: float = 2*60 # s
    bedDiameter: float = 1e-2 #m
    L: float = 10e-2 #m
    C_feed_ref1: float = 0.5 #kg/m**3
    C_feed_ref2: float = 0.5 #kg/m**3
    Pe1_REF: float = 3335.00 #-
    Pe2_REF: float = 2926.00 #-
    volColumn: int = field(init=False)
    tau: int = field(init=False)
    m_injecteds: numpy.ndarray = field(init=False)
    v: float = field(init=False)
    v_eff: float = field(init=False)
    Dax1: float = field(init=False) #calculated from Pe1
    Dax2: float = field(init=False) #calculated from Pe2
    area_cross: float = field(init=False)
    K1: float = 0.051557545560045645
    K2: float = 0.09241005765666498
    qm: float = 93.90387779497868
    params_as_variable = ['K1', 'K2', 'qm']
    def __post_init__(self):
        self.area_cross = (self.bedDiameter/2)**2 *numpy.pi
        self.volColumn = self.area_cross * self.L
        self.tau = self.volColumn*self.epsilon / self.Q_In
        V_inj = self.Q_In * self.injectedInterval
        self.m_injecteds = numpy.array([
            self.C_feed_ref1 * V_inj,
            self.C_feed_ref2 * V_inj,
        ])
        self.v = self.Q_In / self.area_cross
        self.v_eff = self.v / self.epsilon

        self.Dax1 = self.v_eff * self.L / self.Pe1_REF
        self.Dax2 = self.v_eff * self.L / self.Pe2_REF

class modTutorial(daeModel):
    def __init__(self, Name, Parent = None, Description = "", options=None):
        daeModel.__init__(self, Name, Parent, Description)

        self.options = options
        self.ocfems = options['ocfems']

        self.x = daeDomain("x",  self, m, "")
        self.domComps = daeDomain("domComps",  self, dimless, "")

        self.c = daeVariable("c", no_t, self, "c using high resolution upwind scheme", [self.x, self.domComps])
        self.q_star = daeVariable("q_star", no_t, self, "equilibrium adsorbed intensive amount", [self.x, self.domComps])
        self.q = daeVariable("q", no_t, self, "adsorbed intensive amount", [self.x, self.domComps])
        self.c_feed_var = daeVariable("c_feed_var", no_t, self, "", [self.domComps])
        # self.dqdt = daeVariable("dqdt", no_t, self, "rate of adsorption amount", [self.x])

        for key in self.options['params'].params_as_variable:
            setattr(self, key, daeVariable(key, no_t, self, ""))

    def DeclareEquations(self):
        daeModel.DeclareEquations(self)

        params_ext = self.options['params']
        Dax = [params_ext.Dax1, params_ext.Dax2]
        v_eff = params_ext.v_eff #0.01
        c_feed = [params_ext.C_feed_ref1, params_ext.C_feed_ref2]
        voidT = params_ext.epsilon
        qm = self.qm() #from params variables keys
        Ks = [self.K1(), self.K2()]
        injectedInterval = params_ext.injectedInterval #6.0

        # xp = self.x.Points
        Nx = self.x.NumberOfPoints
        Ncomps = self.domComps.NumberOfPoints
        # t  = Time()
        # c = lambda i: self.c(i)
        c_array = numpy.array([
            [
                self.c(i, j)
                for j in range(Ncomps)
            ]
            for i in range(Nx)
        ], dtype=object)

        ocfems = self.ocfems

        d1dx_d2dx_comps = [
            ocfems[j].get_derivatives_d1_d2(c_array[:,j])
            for j in range(Ncomps)
        ]

        # cont_res_comps = [ocfem.get_continuity_residual() for ocfem in ocfems]

        for j in range(Ncomps):
            ocfem = ocfems[j]
            d1dx, d2dx = d1dx_d2dx_comps[j]

            cont_res = ocfem.get_continuity_residual()
            for i in range(len(cont_res)):
                eq = self.CreateEquation("constrains_ocfem({},{})".format(i,j), "")
                eq.Residual = cont_res[i]


            idx_inners = ocfem.get_indexes_inner_points().tolist()

            for i in idx_inners:
                eq = self.CreateEquation("c(%d)" % i, "")
                eq.Residual = dt(c_array[i, j]) - (-v_eff * d1dx[i] + Dax[j]*d2dx[i] - (1-voidT)/voidT * dt(self.q(i, j)))
                eq.CheckUnitsConsistency = False

            # Lower Boundary:
            eq = self.CreateEquation("BC1", "")
            eq.Residual = Dax[j]*d1dx[0] - (v_eff * (c_array[0, j] - self.c_feed_var(j)))

            # Upper Boundary:
            eq = self.CreateEquation("BC2", "")
            eq.Residual = d1dx[-1] - 0.0

            "Isotherm"

            for i in range(Nx):
                eq = self.CreateEquation('q_star({})'.format(i))
                eq.Residual = self.q_star(i, j) - (
                    qm * Ks[j] * c_array[i,j] / (1 + Ks[0] * c_array[i,0] + Ks[1] * c_array[i,1])
                )

            "Forced to match dqdt dqedt"

            for i in range(Nx):
                eq = self.CreateEquation('dqdt({})'.format(i))
                eq.Residual = dt(self.q(i, j)) - (100*(self.q_star(i, j) - self.q(i, j)))
                eq.CheckUnitsConsistency = False

        "Pulse consideration by STATE Transition"

        # self.IF(dae.Time() < dae.Constant(self.injectedInterval()), eventTolerance = 1E-5)
        self.IF(Time()/Constant(1*s) < injectedInterval, eventTolerance = 1E-10)

        for j in range(Ncomps):
            eq = self.CreateEquation("Cfeed{}".format(j), "The pulse is on")
            eq.Residual = self.c_feed_var(j) - c_feed[j]

        self.ELSE()

        for j in range(Ncomps):
            eq = self.CreateEquation("Cfeed{}".format(j), "The pulse is off")
            eq.Residual = self.c_feed_var(j) - Constant(0.0)

        self.END_IF()

        pass

class simTutorial(daeSimulation):
    def __init__(self, ne):

        op_params = OperationParams()


        # ne = 10
        self.L = op_params.L
        h = numpy.linspace(0, self.L, ne + 1)
        ocfem1 = OrtogonalCollocationFiniteElements(ne, 10, h, dtype=adouble)
        ocfem2 = OrtogonalCollocationFiniteElements(ne, 10, h, dtype=adouble)

        self.n_tot_ocfem = ocfem1.n_tot
        print('Total points for OCFEM fullgrid:', self.n_tot_ocfem)

        daeSimulation.__init__(self)


        options = {'ocfems': [ocfem1, ocfem2], 'params': op_params}
        self.m = modTutorial("tutorial_cv_6(%d)" % ne, options=options)
        self.m.Description = __doc__

        self.ne = ne

    def SetUpParametersAndDomains(self):
        self.m.x.CreateStructuredGrid(self.n_tot_ocfem-1, 0.0, self.L)
        self.m.x.Points = self.m.ocfems[0].x_full
        self.m.domComps.CreateArray(2)

    def SetUpVariables(self):
        Nx = self.m.x.NumberOfPoints
        Ncomps = self.m.domComps.NumberOfPoints
        xp = self.m.x.Points
        for j in range(Ncomps):
            idx_inner_points = self.m.ocfems[j].get_indexes_inner_points()
            for i in idx_inner_points.tolist():
                self.m.c.SetInitialCondition(i, j, 0.0)

            for i in range(Nx):
                self.m.q_star.SetInitialCondition(i, j, 0.0)

        for key in self.m.options['params'].params_as_variable:
            val = getattr(self.m.options['params'], key)
            obj_var = getattr(self.m, key)
            obj_var.AssignValue(val)


# Setup everything manually and run in a console
def simulate(ne, **kwargs):
    # Create Log, Solver, DataReporter and Simulation object
    log          = daePythonStdOutLog()
    daesolver    = daeIDAS()
    datareporter = daeDelegateDataReporter()
    simulation   = simTutorial(ne)

    # Do no print progress
    log.PrintProgress = False

    lasolver = pySuperLU.daeCreateSuperLUSolver()

    daesolver.RelativeTolerance = 1e-7

    # 1. TCP/IP
    tcpipDataReporter = daeTCPIPDataReporter()
    datareporter.AddDataReporter(tcpipDataReporter)
    simName = simulation.m.Name + strftime(" [%d.%m.%Y %H:%M:%S]", localtime())
    if not tcpipDataReporter.Connect("", simName):
        sys.exit()

    # 2. Data
    dr = daeNoOpDataReporter()
    datareporter.AddDataReporter(dr)

    t_final = 60*30.0
    daeActivity.simulate(simulation, reportingInterval = t_final*0.01,
                                     timeHorizon       = t_final,
                                     lasolver          = lasolver,
                                     datareporter      = datareporter,
                                     **kwargs)

    ###########################################
    #  Data                                   #
    ###########################################
    results = dr.Process.dictVariables

    cvar = results[simulation.m.Name + '.c']
    keys = [k.split('.')[-1] for k in results]
    dvars = {
        k: results[simulation.m.Name + '.' + k]
        for k in keys
    }
    c    = cvar.Values[-1, :]  # 2D array [t,x]

    return simulation.m.x.Points, dvars, simulation

def run(**kwargs):
    # Nxs = numpy.array([20, 40, 80])
    # N_elements = numpy.array([1, 2, 5, 10, 20, 30])
    # N_elements = numpy.array([10])
    ne = 100
    # n = len(N_elements)
    # hs = L / N_elements
    # c  = []

    # Run simulations
    # for i, ne in enumerate(N_elements):
    nx, dvars, simulation = simulate(int(ne), **kwargs)
        # c.append((nx, c_))

    quit()

    times = dvars['c'].TimeValues
    c = dvars['c'].Values[-1]
    fontsize = 14
    fontsize_legend = 11
    fig = plt.figure(figsize=(12,4), facecolor='white')
    fig.canvas.set_window_title('Tutorial cv_6')

    # for i, ne in enumerate(N_elements):
    #     ax = plt.subplot(2,3,i+1)
    #     plt.figure(1, facecolor='white')
    #     plt.plot(c[i][0], c[i][1], 'ro', linewidth=1.0, label='c (ne=%d)' % ne)
    #     plt.xlabel('x', fontsize=fontsize)
    #     plt.ylabel('c', fontsize=fontsize)
    #     plt.legend(fontsize=fontsize_legend)
    ax = plt.subplot(2,3,1)
    plt.figure(1, facecolor='white')
    plt.plot(nx, c, 'ro', linewidth=1.0, label='c (ne=%d)' % ne)
    plt.xlabel('x', fontsize=fontsize)
    plt.ylabel('c', fontsize=fontsize)
    plt.legend(fontsize=fontsize_legend)

    ax = plt.subplot(2,3,2)
    plt.figure(1, facecolor='white')
    plt.plot(times, dvars['c'].Values[:,-1], '-k', linewidth=2.0, label='c (ne=%d)' % ne)
    plt.xlabel('x', fontsize=fontsize)
    plt.ylabel('c', fontsize=fontsize)
    plt.legend(fontsize=fontsize_legend)

    plt.tight_layout()
    plt.show()

if __name__ == "__main__":
    run()
