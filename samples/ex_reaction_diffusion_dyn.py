import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scikits.odes import dae
from scipy import optimize

from pyocfem import OrtogonalCollocationFiniteElements


def run_dyn_sample():

    ocfem = OrtogonalCollocationFiniteElements(10, 3)
    idxs_algebraics = ocfem.get_algebraic_index()

    model_sample_dyn = create_residual_function(ocfem)

    y0 = ocfem.get_initial_guess() *0.0#* 298.15
    yp0 = np.empty_like(y0)
    # res = model_sample_ss(y0, ocfem)

    SOLVER = 'ida'
    tout = np.linspace(0, 0.5, 101)
    # tout = np.linspace(0, 0.1, 200)

    model_sample_dyn(0, y0, np.zeros_like(y0), yp0)

    solver = dae('ida', model_sample_dyn,
                compute_initcond='yp0',
                #  first_step_size=1e-10,
                atol=1e-6,
                rtol=1e-6,
                algebraic_vars_idx=idxs_algebraics,
                old_api=False)

    solution = solver.solve(tout, y0, yp0)

    if solution.flag < 0:
        print ('Error: ', solution.message, 'Error at time', solution.errors.t)
        quit()


    y_time_mat = solution.values.y
    idx_in_r_grid = ocfem.get_indexes_solution_in_r_grid()
    y_time_mat_adj = y_time_mat[:, idx_in_r_grid]

    plt.figure()
    plt.plot(tout, y_time_mat_adj)

    # sol = optimize.root(model_sample_ss, y0, args=(ocfem,))

    # y_sol = np.hstack((sol.x[0:ocfem.n_nodal], sol.x[ocfem.n_nodal+1:]))

    # print('message', sol.message)
    # print('x\n:', sol.x)


    # plt.figure()
    # plt.plot(ocfem.r,y_sol, 'o')

    return



def create_residual_function(ocfem):


    def residual_model(t, y, ydot, residue):
        n = ocfem.n_grid

        ydot_mat = ydot.reshape((ocfem.ne,-1))
        residue_mat = residue.reshape((ocfem.ne,-1))

        y_mat, d1_mat, d2_mat = ocfem.get_matrix(y)
        cont_res = ocfem.get_continuity_residual()
        # res += cont_res.tolist()
        residue[0:len(cont_res)] = cont_res

        i_low = len(cont_res)

        # Boundary Condition in x= -La
        Pe = 2
        Da = 5

        residue[i_low] = (-1/Pe)*d1_mat[0,0] - (1 - y_mat[0,0])

        # Differential Equation for TA (inner points)
        c = i_low+1#len(cont_res) + 1
        for k in range(ocfem.ne):
            for i in range(1,ocfem.n_nodal-1):
                residue[c] = ydot_mat[k,i] + d1_mat[k,i] - (1/Pe * d2_mat[k,i] - Da*y_mat[k,i])
                c += 1


        # Boundary Conditions in x = 0
        residue[-1] = y_mat[-1,-1] - 0.0

        # # Continuity Residues:
        # residue_mat[0,-1] = cont_res[0]
        # residue_mat[1,-2] = cont_res[1]

        pass
    return residual_model


if __name__ == "__main__":
    run_dyn_sample()

    plt.show()
