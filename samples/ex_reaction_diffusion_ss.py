import os
import numpy as np
import matplotlib.pyplot as plt
from pyocfem import OrtogonalCollocationFiniteElements
from scipy import optimize
import pandas as pd

FILEDIR = os.path.dirname(__file__)

def run_ss_sample():

    ocfem = OrtogonalCollocationFiniteElements(10, 2)

    y0 = ocfem.get_initial_guess()
    res = model_sample_ss(y0, ocfem)

    sol = optimize.root(model_sample_ss, y0, args=(ocfem,))

    y_sol = ocfem.get_solution_vector(sol.x)
    print('message', sol.message)
    print('x\n:', sol.x)

    df_ocm = pd.read_csv(os.path.join(FILEDIR, 'ocm_pe_da_case.csv'), index_col=0)

    plt.figure()
    plt.plot(ocfem.r, y_sol, 'o-', label='ocfem')
    plt.plot(df_ocm['x'], df_ocm['y'], 'r-', label='ocm')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.legend()

    plt.show()
    return


def model_sample_ss(y, ocfem):
    res = []

    y_mat, d1_mat, d2_mat = ocfem.get_matrix(y)
    cont_res = ocfem.get_continuity_residual()
    res += cont_res.tolist()

    # Lower Boundary Condition
    res_aux = y_mat[0,0] - 1.0
    res.append(res_aux)

    # Upper Boundary Condition
    res_aux = d1_mat[-1,-1] - 0.0
    res.append(res_aux)

    # Inner Points
    for k in range(ocfem.ne):
        for i in range(1,ocfem.n_nodal-1):
            res_aux = d2_mat[k,i] - 2*d1_mat[k,i] - 10*y_mat[k,i]
            res.append(res_aux)


    return np.array(res)

if __name__ == "__main__":
    run_ss_sample()
