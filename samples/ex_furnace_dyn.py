import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scikits.odes import dae
from scipy import optimize

from pyocfem import OrtogonalCollocationFiniteElements


def run_dyn_sample():

    ocfem = OrtogonalCollocationFiniteElements(10, 3)
    idxs_algebraics = ocfem.get_algebraic_index()

    model_sample_dyn = create_residual_function(ocfem)

    y0 = np.ones(ocfem.n_tot) * 298.15
    yp0 = np.empty_like(y0)
    # res = model_sample_ss(y0, ocfem)

    SOLVER = 'ida'
    tout = np.linspace(0, 0.5, 101)
    # tout = np.linspace(0, 0.1, 200)

    model_sample_dyn(0, y0, np.zeros_like(y0), yp0)

    solver = dae('ida', model_sample_dyn,
                compute_initcond='yp0',
                #  first_step_size=1e-10,
                atol=1e-6,
                rtol=1e-6,
                algebraic_vars_idx=idxs_algebraics,
                old_api=False)

    solution = solver.solve(tout, y0, yp0)

    if solution.flag < 0:
        print ('Error: ', solution.message, 'Error at time', solution.errors.t)
        quit()


    y_time_mat = solution.values.y
    idx_in_r_grid = ocfem.get_indexes_solution_in_r_grid()
    y_time_mat_adj = y_time_mat[:, idx_in_r_grid]

    plt.figure()
    plt.plot(tout, y_time_mat_adj)

    return



def create_residual_function(ocfem):
    # Parameters
    kA = 50*3600;  kB = 398*3600
    cpA = 900;      cpB = 386
    rhoA = 2697;    rhoB = 8920
    LA = 1;         LB = 1.5
    h1 = 1e2*3600;
    h2 = 1e5*3600
    Tinf1 = 298.15; Tinf2 = 1000

    # Grid and matrices

    def residual_model2(t, y, ydot, residue):
        n = ocfem.n_grid

        ydot_mat = ydot.reshape((ocfem.ne,-1))
        residue_mat = residue.reshape((ocfem.ne,-1))

        y_mat, d1_mat, d2_mat = ocfem.get_matrix(y)
        cont_res = ocfem.get_continuity_residual()
        # res += cont_res.tolist()
        residue[0:len(cont_res)] = cont_res

        i_low = len(cont_res)

        # Boundary Condition in x= -La
        residue[i_low] = -kA*d1_mat[0,0]/LA - h1*(Tinf1 - y_mat[0,0])
        # residue_mat[0,0] = -kA*d1_mat[0,0]/LA - h1*(Tinf1 - y_mat[0,0])

        # Differential Equation for TA (inner points)
        c = i_low+1#len(cont_res) + 1
        for k in range(ocfem.ne):
            for i in range(1,ocfem.n_nodal-1):
                residue[c] = kA/rhoA/cpA*(d2_mat[k,i])/LA**2 - ydot_mat[k,i]
                # aux = kA/rhoA/cpA*(d2_mat[k,i])/LA**2 - ydot_mat[k,i]
                # residue_mat[k,i]
                # residue[c] = aux
                c += 1


        # Boundary Conditions in x = 0
        # residue_mat[-1,-1] = y_mat[-1,-1] - 400.0
        residue[-1] = y_mat[-1,-1] - 400.0

        # # Continuity Residues:
        # residue_mat[0,-1] = cont_res[0]
        # residue_mat[1,-2] = cont_res[1]

        pass

    return residual_model2


if __name__ == "__main__":
    run_dyn_sample()

    plt.show()
