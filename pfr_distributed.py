#%%
import numpy as np
from scipy import integrate
from collocation import collocation_matrix
from scikits.odes import dae
import matplotlib.pyplot as plt
from scipy.integrate import ode

NPTS = 100


def integrate_rk3(t0, ti, y, fun, f_args):
    h = ti - t0
    if f_args is not None:
        k1 = h * fun(t0, y, f_args)
        k2 = h * fun(t0 + h/2, y + k1/2, f_args)
        k3 = h * fun(t0 + h, y - k1 + 2*k2, f_args)
        ynew = y + 1/6*(k1 + 4.*k2 + k3)
    else:
        k1 = h * fun(t0, y)
        k2 = h * fun(t0 + h/2, y + k1/2)
        k3 = h * fun(t0 + h, y - k1 + 2*k2)
        ynew = y + 1/6*(k1 + 4.*k2 + k3)        
    return ynew

def multiples_rk(t0, ti, y, fun, f_args, Nintervals = 100):
    tspan = np.linspace(t0, ti, Nintervals + 1)
    for i in range(len(tspan)-1):
        y = integrate_rk3(tspan[i], tspan[i+1], y, fun, f_args)
    return y

def dyn_solver(fun, tcur, tnext, y0, par = None):
    ynew = multiples_rk(tcur, tnext, y0, fun, par, Nintervals=10)
    return ynew

class pfr():
    def __init__(self, N = 20):
        self.D = 1.0
        self.vz = 1.0
        self.k = 1.0
        self.Cf = 1.0
        self.z0 = 0.0
        self.zf = 1.0
        self.N = N
        self.h = self.get_h()

    def get_h(self):
        return (self.zf - self.z0) / self.N

def solver_setup_base(N):
    par = pfr(N)
    t0 = np.array([0.1])
    y0 = np.zeros(par.N)
    return [t0, y0, par]

def model_pfr(t, y, par):
    rhs = np.empty_like(y)
    N = par.N
    D, vz, k, Cf, h = par.D, par.vz, par.k, par.Cf, par.h
    #dCi = yp
    Ci = y
    aux1 = D / (vz * h)
    C0 = 1.0 / (1.0 + aux1) * (aux1 * Ci[0] + Cf)
    CNp1 = Ci[N - 1]
    aux2 = D / h**2
    aux3 = vz / (2 * h)
    rhs[0] = aux2 * (Ci[1] - 2.0 * Ci[0] + C0) - \
        aux3 * (Ci[1] - C0) + k * Ci[0]
    for i in np.arange(1, N - 1):
        tt1 = aux2 * (Ci[i + 1] - 2.0 * Ci[i] + Ci[i - 1])
        tt2 = -aux3 * (Ci[i + 1] - Ci[i - 1]) + k * Ci[i]
        rhs[i] = tt1 + tt2
    rhs[N - 1] = aux2 * (CNp1 - 2.0 * Ci[N - 1] + Ci[N - 2]) - \
        aux3 * (CNp1 - Ci[N - 2]) + k * Ci[N - 1]
    return rhs

def model_pfr_np(t, y, par):
    rhs = np.empty_like(y)
    N = par.N
    D, vz, k, Cf, h = par.D, par.vz, par.k, par.Cf, par.h
    # dCi = yp
    Ci = y
    aux1 = D / (vz * h)
    C0 = 1.0 / (1.0 + aux1) * (aux1 * Ci[0] + Cf)
    CNp1 = Ci[N - 1]
    aux2 = D / h**2
    aux3 = vz / (2 * h)
    rhs[0] = aux2 * (Ci[1] - 2.0 * Ci[0] + C0) - \
        aux3 * (Ci[1] - C0) + k * Ci[0]
    tt1 = aux2 * (Ci[2:] - 2.0 * Ci[1:-1] + Ci[0:-2])
    tt2 = -aux3 * (Ci[2:] - Ci[0:-2]) + k * Ci[1:-1]
    rhs[1:-1] = tt1 + tt2

    rhs[N - 1] = aux2 * (CNp1 - 2.0 * Ci[N - 1] + Ci[N - 2]) - \
        aux3 * (CNp1 - Ci[N - 2]) + k * Ci[N - 1]
    return rhs    

#%% NUMPY

par = pfr(NPTS)
zspan = np.linspace(par.z0, par.zf, NPTS)
y0 = np.zeros(par.N)
nt = 101
tspan = np.linspace(0.0, 0.05, nt)
y_full = np.zeros((nt, par.N))
y_full[0,:] = y0
y = y0.copy()
for i in range(len(tspan)-1):
    y = dyn_solver(model_pfr_np, tspan[i], tspan[i+1], y, par)
    y_full[i+1,:] = y

#%%
plt.figure()
plt.plot(zspan, y_full[0,:], label='ini')
plt.plot(zspan, y_full[int(np.floor(nt/2)),:], label='interm')
plt.plot(zspan, y_full[-1,:], label='final')
plt.legend()

plt.show()