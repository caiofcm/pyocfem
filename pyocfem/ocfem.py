import numpy as np
from scipy import integrate, optimize
from .collocation import collocation_matrix
# from scikits.odes import dae
import matplotlib.pyplot as plt
# from scikits.odes import dae
# import pandas as pd


class OrtogonalCollocationFiniteElements():

    def __init__(self, ne, n_internal, h=None, dtype=float):
        """Constructor for OtogonalCollocationFIniteElements 1D

        Parameters
        ----------
        ne : int
            Number of finite elements points
        n_internal : int
            Number of internal collocation points
        h : np.ndarray, optional
            Optional position for elements, by default None
            If None will be uniformly spaced.

        Relevant Properties
        ----------
        r : np.ndarray
            The correcly spaced grid for the solution (no intersections)
        n_grid : int
            Number of points in the grid (no intersections)
        n_nodal: int
            Number of points in each element (with boundaries)
        h: np.ndarray
            The array with the limites of the elements
        """
        self.ne = ne
        self.n_internal = n_internal
        self.dtype = dtype
        n_nodal = n_internal + 2
        self.n_nodal = n_nodal
        if h is None:
            h = np.linspace(0.0, 1.0, ne + 1)
        self.h = h
        self.n_tot = self.ne * self.n_nodal

        self.n_grid = self.ne*self.n_nodal - (self.ne-1)

        [A, B, xgrid] = collocation_matrix(n_internal,'2*x-1',[0.0, 1.0])
        self.A = A
        self.B = B
        self.xgrid = xgrid

        self.r = np.empty(self.n_grid)
        for k in range(ne):
            self.r[k*(n_nodal-1):(k+1)*(n_nodal-1)+1] = self.h[k] + xgrid*(self.h[k+1]-self.h[k])

        self.x_full = np.empty(self.n_tot)
        for k in range(ne):
            self.x_full[k*(n_nodal):(k+1)*(n_nodal)] = self.h[k] + xgrid*(self.h[k+1]-self.h[k])

        self.y_mat = np.empty((ne,n_nodal), dtype=dtype)
        self.d1_mat = np.empty((ne,n_nodal), dtype=dtype)
        self.d2_mat = np.empty((ne,n_nodal), dtype=dtype)
        return

    def get_initial_guess(self): #should be set initial guess from the correct grid x
        return np.ones((self.n_nodal*self.ne))

    def get_matrix(self, y):
        # y_mat = self.y_mat
        # d1_mat = self.d1_mat
        # d2_mat = self.d2_mat
        ne = self.ne
        # n_nodal = self.n_nodal
        h = self.h

        self.y_mat = y.reshape((ne,-1))

        # Dif1
        for k in range(ne):
            self.d1_mat[k,:] = np.dot(self.A, self.y_mat[k,:]) * 1/(h[k+1] - h[k])

        # Dif2
        for k in range(ne):
            self.d2_mat[k,:] = np.dot(self.B, self.y_mat[k,:]) * 1/(h[k+1] - h[k])**2

        # try: #numeric
        #     # raise TypeError()
        #     # Dif1
        #     for k in range(ne):
        #         self.d1_mat[k,:] = np.dot(self.A, self.y_mat[k,:]) * 1/(h[k+1] - h[k])

        #     # Dif2
        #     for k in range(ne):
        #         self.d2_mat[k,:] = np.dot(self.B, self.y_mat[k,:]) * 1/(h[k+1] - h[k])**2
        # except TypeError as e: #adouble for instance
        #     # Dif1
        #     for k in range(ne):
        #         for i in range(self.n_nodal):
        #             aux_sum = 0.0
        #             for j in range(self.n_nodal):
        #                 aux_sum = self.A[i, j] * self.y_mat[k, j]
        #             self.d1_mat[k, i] = aux_sum * 1/(h[k+1] - h[k])

        #     # Dif2
        #     for k in range(ne):
        #         for i in range(self.n_nodal):
        #             aux_sum = 0.0
        #             for j in range(self.n_nodal):
        #                 aux_sum = self.B[i, j] * self.y_mat[k, j]
        #             self.d2_mat[k, i] = aux_sum * 1/(h[k+1] - h[k])**2


        return self.y_mat, self.d1_mat, self.d2_mat

    def get_derivatives_d1_d2(self, y):
        "Return the derivatives d1dx and d2dx2 for the full index"
        y_mat, d1_mat, d2_mat = self.get_matrix(y)
        d1dx = d1_mat.flatten()
        d2dx = d2_mat.flatten()
        return d1dx, d2dx

    def get_continuity_residual(self):
        res_continuity = np.empty(2*(self.ne-1), dtype=self.dtype)
        c = 0
        for k in range(self.ne-1):
            res_aux = self.y_mat[k+1,0] - self.y_mat[k,self.n_nodal-1]
            res_continuity[c] = res_aux
            c += 1
            res_continuity[c] = self.d1_mat[k,self.n_nodal-1] - self.d1_mat[k+1,0]
            c += 1
        return res_continuity

    def get_continuity_index(self):
        idxs = [[self.n_nodal*k-1, self.n_nodal*k] for k in range(1, self.ne)]
        return [subitem for item in idxs for subitem in item]

    def get_algebraic_index(self):
        continuity_indexes = self.get_continuity_index()
        return [0] + continuity_indexes + [self.n_tot-1]

    def get_solution_vector(self, y_dae_sol):
        idx_cut_intersections = np.arange(self.ne, dtype=int)[1:] * self.n_nodal
        y_sol = np.delete(y_dae_sol, idx_cut_intersections)
        return y_sol

    def get_indexes_solution_in_r_grid(self):
        i_aux = np.arange(self.ne, dtype=int)[1:] * self.n_nodal
        idx_full = np.arange(self.n_tot)
        idx_cutted = np.delete(idx_full, i_aux)
        return idx_cutted

    def get_indexes_inner_points(self):
        n_inners = (self.n_internal)*self.ne
        i_aux_low = np.arange(self.ne, dtype=int)[1:] * self.n_nodal - 1
        i_aux_up = np.arange(self.ne, dtype=int)[1:] * self.n_nodal
        i_aux = np.hstack((0, i_aux_low, i_aux_up, self.n_tot-1))

        idx_full = np.arange(self.n_tot)
        idx_cutted = np.delete(idx_full, i_aux)
        # idx_inners = np.empty(n_inners, dtype=int)
        # for k in range(self.ne):
        #     idx_inners[k*(self.n_internal): k*(self.n_internal)+self.n_internal] =
        #     for i in range(1,self.n_nodal-1):

        return idx_cutted



# def create_residual_function(ocfem):
#     # Parameters
#     kA = 50*3600;  kB = 398*3600
#     cpA = 900;      cpB = 386
#     rhoA = 2697;    rhoB = 8920
#     LA = 1;         LB = 1.5
#     h1 = 1e2*3600;
#     h2 = 1e5*3600
#     Tinf1 = 298.15; Tinf2 = 1000

#     # Grid and matrices

#     def residual_model2(t, y, ydot, residue):
#         n = ocfem.n_grid

#         ydot_mat = ydot.reshape((ocfem.ne,-1))
#         residue_mat = residue.reshape((ocfem.ne,-1))

#         y_mat, d1_mat, d2_mat = ocfem.get_matrix(y)
#         cont_res = ocfem.get_continuity_residual()
#         # res += cont_res.tolist()
#         residue[0:len(cont_res)] = cont_res

#         i_low = len(cont_res)

#         # Boundary Condition in x= -La
#         residue[i_low] = -kA*d1_mat[0,0]/LA - h1*(Tinf1 - y_mat[0,0])
#         # residue_mat[0,0] = -kA*d1_mat[0,0]/LA - h1*(Tinf1 - y_mat[0,0])

#         # Differential Equation for TA (inner points)
#         c = i_low+1#len(cont_res) + 1
#         for k in range(ocfem.ne):
#             for i in range(1,ocfem.n_nodal-1):
#                 residue[c] = kA/rhoA/cpA*(d2_mat[k,i])/LA**2 - ydot_mat[k,i]
#                 # aux = kA/rhoA/cpA*(d2_mat[k,i])/LA**2 - ydot_mat[k,i]
#                 # residue_mat[k,i]
#                 # residue[c] = aux
#                 c += 1


#         # Boundary Conditions in x = 0
#         # residue_mat[-1,-1] = y_mat[-1,-1] - 400.0
#         residue[-1] = y_mat[-1,-1] - 400.0

#         # # Continuity Residues:
#         # residue_mat[0,-1] = cont_res[0]
#         # residue_mat[1,-2] = cont_res[1]

#         pass

#     def residual_model(t, y, ydot, residue):
#         n = ocfem.n_grid

#         ydot_mat = ydot.reshape((ocfem.ne,-1))
#         residue_mat = residue.reshape((ocfem.ne,-1))

#         y_mat, d1_mat, d2_mat = ocfem.get_matrix(y)
#         cont_res = ocfem.get_continuity_residual()
#         # res += cont_res.tolist()
#         residue[0:len(cont_res)] = cont_res

#         i_low = len(cont_res)

#         # Boundary Condition in x= -La
#         Pe = 2
#         Da = 5

#         residue[i_low] = (-1/Pe)*d1_mat[0,0] - (1 - y_mat[0,0])

#         # Differential Equation for TA (inner points)
#         c = i_low+1#len(cont_res) + 1
#         for k in range(ocfem.ne):
#             for i in range(1,ocfem.n_nodal-1):
#                 residue[c] = ydot_mat[k,i] + d1_mat[k,i] - (1/Pe * d2_mat[k,i] - Da*y_mat[k,i])
#                 c += 1


#         # Boundary Conditions in x = 0
#         residue[-1] = y_mat[-1,-1] - 0.0

#         # # Continuity Residues:
#         # residue_mat[0,-1] = cont_res[0]
#         # residue_mat[1,-2] = cont_res[1]

#         pass
#     return residual_model

# def run_ss_sample():

#     ocfem = OrtogonalCollocationFiniteElements(10, 2)

#     y0 = ocfem.get_initial_guess()
#     res = model_sample_ss(y0, ocfem)

#     sol = optimize.root(model_sample_ss, y0, args=(ocfem,))

#     y_sol = ocfem.get_solution_vector(sol.x)
#     print('message', sol.message)
#     print('x\n:', sol.x)

#     df_ocm = pd.read_csv('ocm_pe_da_case.csv', index_col=0)

#     plt.figure()
#     plt.plot(ocfem.r, y_sol, 'o-', label='ocfem')
#     plt.plot(df_ocm['x'], df_ocm['y'], 'r-', label='ocm')
#     plt.xlabel('x')
#     plt.ylabel('y')
#     plt.legend()

#     plt.show()
#     return


if __name__ == "__main__":

    # run_ss_sample()
    # run_dyn_sample()

    # plt.show()

    pass
    #END
