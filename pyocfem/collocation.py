import numpy as np
import sympy
from sympy.parsing.sympy_parser import parse_expr

def calc_collocation_grid_legendre(n, expr='2*x-1', bounds=None):
    x = sympy.symbols('x')
    expr_parsed = parse_expr(expr)
    leg_poly_fn = sympy.legendre(n, expr_parsed)
    solution = sympy.solve(leg_poly_fn)
    xgrid = np.array([float(v.evalf()) for v in solution])
    if bounds:
        if len(bounds) != 2:
            raise AttributeError('Bounds should be size = 2')
        if np.isnan(bounds[0]):
            xgrid = np.hstack((xgrid, bounds[1]))
        elif np.isnan(bounds[1]):
            xgrid = np.hstack((bounds[0], xgrid))    
        else:
            xgrid = np.hstack((bounds[0], xgrid, bounds[1]))
    xgrid = np.sort(xgrid)
    return xgrid

def calc_collocation_matrix(xgrid):
    n = len(xgrid)
    A = np.zeros((n,n))
    v = np.zeros(n)
    for i in range(n):
        p = 1
        for j in range(n):
            fat = xgrid[i] - xgrid[j]
            v[i] = fat*v[i] + p
            p = fat*p

    for i in range(n):
        for j in range(n):
            if i==j:
                continue
            A[i,j] = v[i]/v[j]/(xgrid[i] - xgrid[j])
            A[i,i] = A[i,i] - A[i,j] #?

    B = np.dot(A,A)
    return A, B

def calc_collocation_matrix_AB(xgrid):
    "older"
    # if n is None:
    #     n = len(xgrid)
    n = len(xgrid)
    dpnodal = np.zeros(n)
    d2pnodal = np.zeros(n)
    d3pnodal = np.zeros(n)
    for i in range(n):
        pi, dpi, d2pi, d3pi = 1.0, 0.0, 0.0, 0.0
        for j in range(n):
            dxi = (xgrid[i] - xgrid[j])
            d3pi = (dxi*d3pi) + (3.0*d2pi)
            d2pi = (dxi*d2pi) + (2.0*dpi)
            dpi = (dxi*dpi) + pi
            pi = (dxi*pi)
        dpnodal[i] = dpi
        d2pnodal[i] = d2pi
        d3pnodal[i] = d3pi

    A = np.empty((n,n))
    B = np.empty((n,n))
    for i in range(n):
        A[i,i] = d2pnodal[i]/(2.0*dpnodal[i])
        B[i,i] = d3pnodal[i]/(3.0*dpnodal[i])

        for j in range(n):
            if j == i:
                continue
            A[i,j] = dpnodal[i]/((xgrid[i]-xgrid[j])*dpnodal[j])
            B[i,j] = 2.0*A[i,j]*(A[i,i] - (1.0/(xgrid[i]-xgrid[j])))

    return A, B

def collocation_matrix(n, expr='2*x-1', bounds=None):
    xgrid = calc_collocation_grid_legendre(n, expr, bounds)
    A, B = calc_collocation_matrix(xgrid)
    # A, B = calc_collocation_matrix_AB(xgrid)
    return A, B, xgrid

def test_grid():
    xgrid = calc_collocation_grid_legendre(2, bounds=[0.0,1.0])
    A, B = calc_collocation_matrix(xgrid)
    # A, B = calc_collocation_matrix_AB(xgrid)
    print('A')
    print(A)
    print('B')
    print(B)
    print('A.**2')
    print(A**2)
    print('np.dot(A,A)')
    print(np.dot(A,A))
    pass


if __name__ == "__main__":
    test_grid()